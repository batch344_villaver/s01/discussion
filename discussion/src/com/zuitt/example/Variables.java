package com.zuitt.example;
// A "package" in Java is used to group related classes. Think of it as a folder in a file directory.
// One Java file = one Java class
    // Packages are divided into 2 categories:
        // 1. Built-in packages (Packages from the Java APi -- OpenJDK/pre-built)
        // 2. User-defined packages (created packages by the programmer)

// Package creation in Java follows the Reverse Domain Name Notation for the naming convention

public class Variables {
    public static void main (String[] args) {
        // Naming convention
            // the terminology used for variable names is "identifier"
            // All identifiers should begin with a letter (A to Z, a to z), currency ($), or an underscore
            // After the first character, identifiers can have any combination of characters
            // Most importantly, identifiers are case-sensitive

        // Variable declaration:
        int age;
        char middleName;

        // Variable declaration with initialization:
        int x;
        int y = 0;

        // Initialization after declaration:
        x = 1;

        // Output to the system:
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        // Primitive Data types:
            // predefined within the Java Programming Language which is used for single-valued variables with limited capabilities
        // int -- whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        // long -- L is added to the end of the long number to be recognized
        long worldPopulation = 46582028474565L;
        System.out.println(worldPopulation);

        // Floating value
        // float -- add f at the end of the value
        // up to 7 decimal places
        float piFloat = 3.14159265359f;
        System.out.println("The value of piFloat is " + piFloat);

        // double -- floating values. Holds 18 decimal places
        double piDouble = 3.14159265359;
        System.out.println("The value of piDouble is " + piDouble);

        // char -- single character
        // uses single quote
        char letter = 'a';
        System.out.println(letter);

        // boolean -- true or false value
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        // constants -- Java uses the "final" keyword so that a variable's value cannot be changed/reassigned
        // use UPPERCASE in naming constant identifiers to easily identify
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);


        // Non-Primitive Data
            // also known as reference data types refers to instances or objects
            // does not directly store the value of a variable, but rather remember the reference to that variable

        // String
        // stores a sequence or array of characters
        // String are actually objects that can use methods

        String username = "JSmith";
        System.out.println(username);

        // sample string method
        int stringLength = username.length();
        System.out.println(stringLength);
    }
}
